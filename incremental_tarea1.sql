# The proper term is pseudo_replica_mode, but we use this compatibility alias
# to make the statement usable on server versions 8.0.24 and older.
/*!50530 SET @@SESSION.PSEUDO_SLAVE_MODE=1*/;
/*!50003 SET @OLD_COMPLETION_TYPE=@@COMPLETION_TYPE,COMPLETION_TYPE=0*/;
DELIMITER /*!*/;
# at 4
#211214  5:39:19 server id 1  end_log_pos 125 CRC32 0x9e20d414 	Start: binlog v 4, server v 8.0.27-0ubuntu0.20.04.1 created 211214  5:39:19
BINLOG '
By64YQ8BAAAAeQAAAH0AAAAAAAQAOC4wLjI3LTB1YnVudHUwLjIwLjA0LjEAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAEwANAAgAAAAABAAEAAAAYQAEGggAAAAICAgCAAAACgoKKioAEjQA
CigBFNQgng==
'/*!*/;
# at 125
#211214  5:39:19 server id 1  end_log_pos 156 CRC32 0x8f7f1c60 	Previous-GTIDs
# [empty]
# at 156
#211214  5:39:42 server id 1  end_log_pos 235 CRC32 0x762cf7fb 	Anonymous_GTID	last_committed=0	sequence_number=1	rbr_only=yes	original_committed_timestamp=1639460382365233	immediate_commit_timestamp=1639460382365233	transaction_length=359
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639460382365233 (2021-12-14 05:39:42.365233 UTC)
# immediate_commit_timestamp=1639460382365233 (2021-12-14 05:39:42.365233 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639460382365233*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 235
#211214  5:39:42 server id 1  end_log_pos 330 CRC32 0x5903a049 	Query	thread_id=422	exec_time=0	error_code=0
SET TIMESTAMP=1639460382/*!*/;
SET @@session.pseudo_thread_id=422/*!*/;
SET @@session.foreign_key_checks=1, @@session.sql_auto_is_null=0, @@session.unique_checks=1, @@session.autocommit=1/*!*/;
SET @@session.sql_mode=1168113696/*!*/;
SET @@session.auto_increment_increment=1, @@session.auto_increment_offset=1/*!*/;
/*!\C utf8mb4 *//*!*/;
SET @@session.character_set_client=255,@@session.collation_connection=255,@@session.collation_server=255/*!*/;
SET @@session.time_zone='SYSTEM'/*!*/;
SET @@session.lc_time_names=0/*!*/;
SET @@session.collation_database=DEFAULT/*!*/;
/*!80011 SET @@session.default_collation_for_utf8mb4=255*//*!*/;
BEGIN
/*!*/;
# at 330
#211214  5:39:42 server id 1  end_log_pos 412 CRC32 0x9c9256fb 	Table_map: `TAREA1_200915518`.`tarea1_200915518` mapped to number 239
# at 412
#211214  5:39:42 server id 1  end_log_pos 484 CRC32 0xf7ee8ce4 	Write_rows: table id 239 flags: STMT_END_F

BINLOG '
Hi64YRMBAAAAUgAAAJwBAAAAAO8AAAAAAAEAEFRBUkVBMV8yMDA5MTU1MTgAEHRhcmVhMV8yMDA5
MTU1MTgAAwMPEgOHAAAGAQEAAgFT+1aSnA==
Hi64YR4BAAAASAAAAOQBAAAAAO8AAAAAAAEAAgAD/wAKAAAAGkluY3JlbWVudGFsIFdhbHRlciBN
b3JhbGVzmatcWerkjO73
'/*!*/;
### INSERT INTO `TAREA1_200915518`.`tarea1_200915518`
### SET
###   @1=10
###   @2='Incremental Walter Morales'
###   @3='2021-12-14 05:39:42'
# at 484
#211214  5:39:42 server id 1  end_log_pos 515 CRC32 0x01f53767 	Xid = 702467
COMMIT/*!*/;
# at 515
#211214  5:39:45 server id 1  end_log_pos 594 CRC32 0x434cdcf5 	Anonymous_GTID	last_committed=1	sequence_number=2	rbr_only=yes	original_committed_timestamp=1639460385460366	immediate_commit_timestamp=1639460385460366	transaction_length=363
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639460385460366 (2021-12-14 05:39:45.460366 UTC)
# immediate_commit_timestamp=1639460385460366 (2021-12-14 05:39:45.460366 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639460385460366*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 594
#211214  5:39:45 server id 1  end_log_pos 689 CRC32 0x03920a49 	Query	thread_id=422	exec_time=0	error_code=0
SET TIMESTAMP=1639460385/*!*/;
BEGIN
/*!*/;
# at 689
#211214  5:39:45 server id 1  end_log_pos 771 CRC32 0xd13d13af 	Table_map: `TAREA1_200915518`.`tarea1_200915518` mapped to number 239
# at 771
#211214  5:39:45 server id 1  end_log_pos 847 CRC32 0x44a1ee14 	Write_rows: table id 239 flags: STMT_END_F

BINLOG '
IS64YRMBAAAAUgAAAAMDAAAAAO8AAAAAAAEAEFRBUkVBMV8yMDA5MTU1MTgAEHRhcmVhMV8yMDA5
MTU1MTgAAwMPEgOHAAAGAQEAAgFTrxM90Q==
IS64YR4BAAAATAAAAE8DAAAAAO8AAAAAAAEAAgAD/wALAAAAHkluY3JlbWVudGFsIEFsZWphbmRy
YSBQYWxhY2lvc5mrXFntFO6hRA==
'/*!*/;
### INSERT INTO `TAREA1_200915518`.`tarea1_200915518`
### SET
###   @1=11
###   @2='Incremental Alejandra Palacios'
###   @3='2021-12-14 05:39:45'
# at 847
#211214  5:39:45 server id 1  end_log_pos 878 CRC32 0xd3e175e4 	Xid = 702469
COMMIT/*!*/;
# at 878
#211214  5:39:48 server id 1  end_log_pos 957 CRC32 0x6d955e78 	Anonymous_GTID	last_committed=2	sequence_number=3	rbr_only=yes	original_committed_timestamp=1639460388460216	immediate_commit_timestamp=1639460388460216	transaction_length=355
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639460388460216 (2021-12-14 05:39:48.460216 UTC)
# immediate_commit_timestamp=1639460388460216 (2021-12-14 05:39:48.460216 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639460388460216*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 957
#211214  5:39:48 server id 1  end_log_pos 1052 CRC32 0xaebbe447 	Query	thread_id=422	exec_time=0	error_code=0
SET TIMESTAMP=1639460388/*!*/;
BEGIN
/*!*/;
# at 1052
#211214  5:39:48 server id 1  end_log_pos 1134 CRC32 0xd71c0051 	Table_map: `TAREA1_200915518`.`tarea1_200915518` mapped to number 239
# at 1134
#211214  5:39:48 server id 1  end_log_pos 1202 CRC32 0xd190d02f 	Write_rows: table id 239 flags: STMT_END_F

BINLOG '
JC64YRMBAAAAUgAAAG4EAAAAAO8AAAAAAAEAEFRBUkVBMV8yMDA5MTU1MTgAEHRhcmVhMV8yMDA5
MTU1MTgAAwMPEgOHAAAGAQEAAgFTUQAc1w==
JC64YR4BAAAARAAAALIEAAAAAO8AAAAAAAEAAgAD/wAMAAAAFkluY3JlbWVudGFsIEp1YW4gU29s
aXOZq1xZ8C/QkNE=
'/*!*/;
### INSERT INTO `TAREA1_200915518`.`tarea1_200915518`
### SET
###   @1=12
###   @2='Incremental Juan Solis'
###   @3='2021-12-14 05:39:48'
# at 1202
#211214  5:39:48 server id 1  end_log_pos 1233 CRC32 0x9d4b1b65 	Xid = 702471
COMMIT/*!*/;
# at 1233
#211214  5:39:51 server id 1  end_log_pos 1312 CRC32 0x115197e2 	Anonymous_GTID	last_committed=3	sequence_number=4	rbr_only=yes	original_committed_timestamp=1639460391260619	immediate_commit_timestamp=1639460391260619	transaction_length=362
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639460391260619 (2021-12-14 05:39:51.260619 UTC)
# immediate_commit_timestamp=1639460391260619 (2021-12-14 05:39:51.260619 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639460391260619*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 1312
#211214  5:39:51 server id 1  end_log_pos 1407 CRC32 0x9a78c309 	Query	thread_id=422	exec_time=0	error_code=0
SET TIMESTAMP=1639460391/*!*/;
BEGIN
/*!*/;
# at 1407
#211214  5:39:51 server id 1  end_log_pos 1489 CRC32 0x633633f1 	Table_map: `TAREA1_200915518`.`tarea1_200915518` mapped to number 239
# at 1489
#211214  5:39:51 server id 1  end_log_pos 1564 CRC32 0x4912bbd0 	Write_rows: table id 239 flags: STMT_END_F

BINLOG '
Jy64YRMBAAAAUgAAANEFAAAAAO8AAAAAAAEAEFRBUkVBMV8yMDA5MTU1MTgAEHRhcmVhMV8yMDA5
MTU1MTgAAwMPEgOHAAAGAQEAAgFT8TM2Yw==
Jy64YR4BAAAASwAAABwGAAAAAO8AAAAAAAEAAgAD/wANAAAAHUluY3JlbWVudGFsIFZpY2VudGUg
RmVybmFuZGV6matcWfPQuxJJ
'/*!*/;
### INSERT INTO `TAREA1_200915518`.`tarea1_200915518`
### SET
###   @1=13
###   @2='Incremental Vicente Fernandez'
###   @3='2021-12-14 05:39:51'
# at 1564
#211214  5:39:51 server id 1  end_log_pos 1595 CRC32 0x86902b8f 	Xid = 702473
COMMIT/*!*/;
# at 1595
#211214  5:39:54 server id 1  end_log_pos 1674 CRC32 0xba582f36 	Anonymous_GTID	last_committed=4	sequence_number=5	rbr_only=yes	original_committed_timestamp=1639460394233870	immediate_commit_timestamp=1639460394233870	transaction_length=357
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639460394233870 (2021-12-14 05:39:54.233870 UTC)
# immediate_commit_timestamp=1639460394233870 (2021-12-14 05:39:54.233870 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639460394233870*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 1674
#211214  5:39:54 server id 1  end_log_pos 1769 CRC32 0x1afe290a 	Query	thread_id=422	exec_time=0	error_code=0
SET TIMESTAMP=1639460394/*!*/;
BEGIN
/*!*/;
# at 1769
#211214  5:39:54 server id 1  end_log_pos 1851 CRC32 0xa5eebcfb 	Table_map: `TAREA1_200915518`.`tarea1_200915518` mapped to number 239
# at 1851
#211214  5:39:54 server id 1  end_log_pos 1921 CRC32 0xfe16eabd 	Write_rows: table id 239 flags: STMT_END_F

BINLOG '
Ki64YRMBAAAAUgAAADsHAAAAAO8AAAAAAAEAEFRBUkVBMV8yMDA5MTU1MTgAEHRhcmVhMV8yMDA5
MTU1MTgAAwMPEgOHAAAGAQEAAgFT+7zupQ==
Ki64YR4BAAAARgAAAIEHAAAAAO8AAAAAAAEAAgAD/wAOAAAAGEluY3JlbWVudGFsIEp1YW4gR2Fi
cmllbJmrXFn2veoW/g==
'/*!*/;
### INSERT INTO `TAREA1_200915518`.`tarea1_200915518`
### SET
###   @1=14
###   @2='Incremental Juan Gabriel'
###   @3='2021-12-14 05:39:54'
# at 1921
#211214  5:39:54 server id 1  end_log_pos 1952 CRC32 0x7e95bb9a 	Xid = 702475
COMMIT/*!*/;
# at 1952
#211214  5:40:08 server id 1  end_log_pos 1996 CRC32 0x463a2ae2 	Rotate to binlog.000027  pos: 4
SET @@SESSION.GTID_NEXT= 'AUTOMATIC' /* added by mysqlbinlog */ /*!*/;
DELIMITER ;
# End of log file
/*!50003 SET COMPLETION_TYPE=@OLD_COMPLETION_TYPE*/;
/*!50530 SET @@SESSION.PSEUDO_SLAVE_MODE=0*/;

# The proper term is pseudo_replica_mode, but we use this compatibility alias
# to make the statement usable on server versions 8.0.24 and older.
/*!50530 SET @@SESSION.PSEUDO_SLAVE_MODE=1*/;
/*!50003 SET @OLD_COMPLETION_TYPE=@@COMPLETION_TYPE,COMPLETION_TYPE=0*/;
DELIMITER /*!*/;
# at 4
#211214 15:52:59 server id 1  end_log_pos 125 CRC32 0x04360af8 	Start: binlog v 4, server v 8.0.27-0ubuntu0.20.04.1 created 211214 15:52:59
BINLOG '
2724YQ8BAAAAeQAAAH0AAAAAAAQAOC4wLjI3LTB1YnVudHUwLjIwLjA0LjEAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAEwANAAgAAAAABAAEAAAAYQAEGggAAAAICAgCAAAACgoKKioAEjQA
CigB+Ao2BA==
'/*!*/;
# at 125
#211214 15:52:59 server id 1  end_log_pos 156 CRC32 0x449d8f03 	Previous-GTIDs
# [empty]
# at 156
#211214 15:53:39 server id 1  end_log_pos 235 CRC32 0x3672a6ba 	Anonymous_GTID	last_committed=0	sequence_number=1	rbr_only=yes	original_committed_timestamp=1639497219213987	immediate_commit_timestamp=1639497219213987	transaction_length=322
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497219213987 (2021-12-14 15:53:39.213987 UTC)
# immediate_commit_timestamp=1639497219213987 (2021-12-14 15:53:39.213987 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497219213987*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 235
#211214 15:53:39 server id 1  end_log_pos 319 CRC32 0x90f1af4d 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497219/*!*/;
SET @@session.pseudo_thread_id=486/*!*/;
SET @@session.foreign_key_checks=1, @@session.sql_auto_is_null=0, @@session.unique_checks=1, @@session.autocommit=1/*!*/;
SET @@session.sql_mode=1168113696/*!*/;
SET @@session.auto_increment_increment=1, @@session.auto_increment_offset=1/*!*/;
/*!\C utf8mb4 *//*!*/;
SET @@session.character_set_client=255,@@session.collation_connection=255,@@session.collation_server=255/*!*/;
SET @@session.lc_time_names=0/*!*/;
SET @@session.collation_database=DEFAULT/*!*/;
/*!80011 SET @@session.default_collation_for_utf8mb4=255*//*!*/;
BEGIN
/*!*/;
# at 319
#211214 15:53:39 server id 1  end_log_pos 388 CRC32 0x762742cf 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 388
#211214 15:53:39 server id 1  end_log_pos 447 CRC32 0x0ddbf184 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
A764YRMBAAAARQAAAIQBAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVPPQid2
A764YR4BAAAAOwAAAL8BAAAAAPEAAAAAAAEAAgAC/wABAAAAElNhbGEgZGUgZXhhbWVuZXMgMYTx
2w0=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=1
###   @2='Sala de examenes 1'
# at 447
#211214 15:53:39 server id 1  end_log_pos 478 CRC32 0x61682193 	Xid = 702827
COMMIT/*!*/;
# at 478
#211214 15:53:39 server id 1  end_log_pos 557 CRC32 0x7f0739d1 	Anonymous_GTID	last_committed=1	sequence_number=2	rbr_only=yes	original_committed_timestamp=1639497219666035	immediate_commit_timestamp=1639497219666035	transaction_length=322
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497219666035 (2021-12-14 15:53:39.666035 UTC)
# immediate_commit_timestamp=1639497219666035 (2021-12-14 15:53:39.666035 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497219666035*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 557
#211214 15:53:39 server id 1  end_log_pos 641 CRC32 0xbbdcd0c1 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497219/*!*/;
BEGIN
/*!*/;
# at 641
#211214 15:53:39 server id 1  end_log_pos 710 CRC32 0x8967d675 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 710
#211214 15:53:39 server id 1  end_log_pos 769 CRC32 0x3c6c6c93 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
A764YRMBAAAARQAAAMYCAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVN11meJ
A764YR4BAAAAOwAAAAEDAAAAAPEAAAAAAAEAAgAC/wACAAAAElNhbGEgZGUgZXhhbWVuZXMgMpNs
bDw=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=2
###   @2='Sala de examenes 2'
# at 769
#211214 15:53:39 server id 1  end_log_pos 800 CRC32 0x4ec8b01c 	Xid = 702833
COMMIT/*!*/;
# at 800
#211214 15:53:40 server id 1  end_log_pos 879 CRC32 0x8c1085f2 	Anonymous_GTID	last_committed=2	sequence_number=3	rbr_only=yes	original_committed_timestamp=1639497220114751	immediate_commit_timestamp=1639497220114751	transaction_length=322
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497220114751 (2021-12-14 15:53:40.114751 UTC)
# immediate_commit_timestamp=1639497220114751 (2021-12-14 15:53:40.114751 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497220114751*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 879
#211214 15:53:40 server id 1  end_log_pos 963 CRC32 0xee2fd2b3 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497220/*!*/;
BEGIN
/*!*/;
# at 963
#211214 15:53:40 server id 1  end_log_pos 1032 CRC32 0x1a1523d4 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 1032
#211214 15:53:40 server id 1  end_log_pos 1091 CRC32 0x1e3cfc20 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
BL64YRMBAAAARQAAAAgEAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVPUIxUa
BL64YR4BAAAAOwAAAEMEAAAAAPEAAAAAAAEAAgAC/wADAAAAElNhbGEgZGUgZXhhbWVuZXMgMyD8
PB4=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=3
###   @2='Sala de examenes 3'
# at 1091
#211214 15:53:40 server id 1  end_log_pos 1122 CRC32 0x87d6ca19 	Xid = 702839
COMMIT/*!*/;
# at 1122
#211214 15:53:40 server id 1  end_log_pos 1201 CRC32 0x3680a8b4 	Anonymous_GTID	last_committed=3	sequence_number=4	rbr_only=yes	original_committed_timestamp=1639497220562453	immediate_commit_timestamp=1639497220562453	transaction_length=322
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497220562453 (2021-12-14 15:53:40.562453 UTC)
# immediate_commit_timestamp=1639497220562453 (2021-12-14 15:53:40.562453 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497220562453*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 1201
#211214 15:53:40 server id 1  end_log_pos 1285 CRC32 0x581f93ba 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497220/*!*/;
BEGIN
/*!*/;
# at 1285
#211214 15:53:40 server id 1  end_log_pos 1354 CRC32 0xaa595609 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 1354
#211214 15:53:40 server id 1  end_log_pos 1413 CRC32 0xbbeb99a2 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
BL64YRMBAAAARQAAAEoFAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVMJVlmq
BL64YR4BAAAAOwAAAIUFAAAAAPEAAAAAAAEAAgAC/wAEAAAAElNhbGEgZGUgZXhhbWVuZXMgNKKZ
67s=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=4
###   @2='Sala de examenes 4'
# at 1413
#211214 15:53:40 server id 1  end_log_pos 1444 CRC32 0xf1885cf1 	Xid = 702845
COMMIT/*!*/;
# at 1444
#211214 15:53:41 server id 1  end_log_pos 1523 CRC32 0x4d030d58 	Anonymous_GTID	last_committed=4	sequence_number=5	rbr_only=yes	original_committed_timestamp=1639497221016126	immediate_commit_timestamp=1639497221016126	transaction_length=322
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497221016126 (2021-12-14 15:53:41.016126 UTC)
# immediate_commit_timestamp=1639497221016126 (2021-12-14 15:53:41.016126 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497221016126*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 1523
#211214 15:53:41 server id 1  end_log_pos 1607 CRC32 0xd6ee9b88 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497221/*!*/;
BEGIN
/*!*/;
# at 1607
#211214 15:53:41 server id 1  end_log_pos 1676 CRC32 0x21169cc9 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 1676
#211214 15:53:41 server id 1  end_log_pos 1735 CRC32 0x1c789190 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
Bb64YRMBAAAARQAAAIwGAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVPJnBYh
Bb64YR4BAAAAOwAAAMcGAAAAAPEAAAAAAAEAAgAC/wAFAAAAElNhbGEgZGUgaW1hZ2VuZXMgMZCR
eBw=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=5
###   @2='Sala de imagenes 1'
# at 1735
#211214 15:53:41 server id 1  end_log_pos 1766 CRC32 0x68f81e5a 	Xid = 702851
COMMIT/*!*/;
# at 1766
#211214 15:53:41 server id 1  end_log_pos 1845 CRC32 0xb5c22840 	Anonymous_GTID	last_committed=5	sequence_number=6	rbr_only=yes	original_committed_timestamp=1639497221468567	immediate_commit_timestamp=1639497221468567	transaction_length=328
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497221468567 (2021-12-14 15:53:41.468567 UTC)
# immediate_commit_timestamp=1639497221468567 (2021-12-14 15:53:41.468567 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497221468567*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 1845
#211214 15:53:41 server id 1  end_log_pos 1929 CRC32 0xf71aa8db 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497221/*!*/;
BEGIN
/*!*/;
# at 1929
#211214 15:53:41 server id 1  end_log_pos 1998 CRC32 0x915ae914 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 1998
#211214 15:53:41 server id 1  end_log_pos 2063 CRC32 0x5e14eb9b 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
Bb64YRMBAAAARQAAAM4HAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVMU6VqR
Bb64YR4BAAAAQQAAAA8IAAAAAPEAAAAAAAEAAgAC/wAGAAAAGFNhbGEgZGUgcHJvY2VkaW1pZW50
b3MgMZvrFF4=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=6
###   @2='Sala de procedimientos 1'
# at 2063
#211214 15:53:41 server id 1  end_log_pos 2094 CRC32 0xf24d0807 	Xid = 702857
COMMIT/*!*/;
# at 2094
#211214 15:53:41 server id 1  end_log_pos 2173 CRC32 0xb7ceeb97 	Anonymous_GTID	last_committed=6	sequence_number=7	rbr_only=yes	original_committed_timestamp=1639497221920692	immediate_commit_timestamp=1639497221920692	transaction_length=328
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497221920692 (2021-12-14 15:53:41.920692 UTC)
# immediate_commit_timestamp=1639497221920692 (2021-12-14 15:53:41.920692 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497221920692*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 2173
#211214 15:53:41 server id 1  end_log_pos 2257 CRC32 0x723d7980 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497221/*!*/;
BEGIN
/*!*/;
# at 2257
#211214 15:53:41 server id 1  end_log_pos 2326 CRC32 0x54befea2 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 2326
#211214 15:53:41 server id 1  end_log_pos 2391 CRC32 0xfae3caca 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
Bb64YRMBAAAARQAAABYJAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVOi/r5U
Bb64YR4BAAAAQQAAAFcJAAAAAPEAAAAAAAEAAgAC/wAHAAAAGFNhbGEgZGUgcHJvY2VkaW1pZW50
b3MgMsrK4/o=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=7
###   @2='Sala de procedimientos 2'
# at 2391
#211214 15:53:41 server id 1  end_log_pos 2422 CRC32 0xfde60dae 	Xid = 702863
COMMIT/*!*/;
# at 2422
#211214 15:53:42 server id 1  end_log_pos 2501 CRC32 0x48b55ca6 	Anonymous_GTID	last_committed=7	sequence_number=8	rbr_only=yes	original_committed_timestamp=1639497222376229	immediate_commit_timestamp=1639497222376229	transaction_length=328
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497222376229 (2021-12-14 15:53:42.376229 UTC)
# immediate_commit_timestamp=1639497222376229 (2021-12-14 15:53:42.376229 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497222376229*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 2501
#211214 15:53:42 server id 1  end_log_pos 2585 CRC32 0x34fd409f 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497222/*!*/;
BEGIN
/*!*/;
# at 2585
#211214 15:53:42 server id 1  end_log_pos 2654 CRC32 0xdaad8f83 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 2654
#211214 15:53:42 server id 1  end_log_pos 2719 CRC32 0x6ffa99d7 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
Br64YRMBAAAARQAAAF4KAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVODj63a
Br64YR4BAAAAQQAAAJ8KAAAAAPEAAAAAAAEAAgAC/wAIAAAAGFNhbGEgZGUgcHJvY2VkaW1pZW50
b3MgM9eZ+m8=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=8
###   @2='Sala de procedimientos 3'
# at 2719
#211214 15:53:42 server id 1  end_log_pos 2750 CRC32 0xef506765 	Xid = 702869
COMMIT/*!*/;
# at 2750
#211214 15:53:42 server id 1  end_log_pos 2829 CRC32 0x02197d93 	Anonymous_GTID	last_committed=8	sequence_number=9	rbr_only=yes	original_committed_timestamp=1639497222826672	immediate_commit_timestamp=1639497222826672	transaction_length=328
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497222826672 (2021-12-14 15:53:42.826672 UTC)
# immediate_commit_timestamp=1639497222826672 (2021-12-14 15:53:42.826672 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497222826672*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 2829
#211214 15:53:42 server id 1  end_log_pos 2913 CRC32 0x5537cc27 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497222/*!*/;
BEGIN
/*!*/;
# at 2913
#211214 15:53:42 server id 1  end_log_pos 2982 CRC32 0xb7d574f8 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 2982
#211214 15:53:42 server id 1  end_log_pos 3047 CRC32 0x1799fe1b 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
Br64YRMBAAAARQAAAKYLAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVP4dNW3
Br64YR4BAAAAQQAAAOcLAAAAAPEAAAAAAAEAAgAC/wAJAAAAGFNhbGEgZGUgcHJvY2VkaW1pZW50
b3MgNBv+mRc=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=9
###   @2='Sala de procedimientos 4'
# at 3047
#211214 15:53:42 server id 1  end_log_pos 3078 CRC32 0x41c31fea 	Xid = 702875
COMMIT/*!*/;
# at 3078
#211214 15:53:43 server id 1  end_log_pos 3157 CRC32 0xa1871f7a 	Anonymous_GTID	last_committed=9	sequence_number=10	rbr_only=yes	original_committed_timestamp=1639497223283338	immediate_commit_timestamp=1639497223283338	transaction_length=313
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497223283338 (2021-12-14 15:53:43.283338 UTC)
# immediate_commit_timestamp=1639497223283338 (2021-12-14 15:53:43.283338 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497223283338*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 3157
#211214 15:53:43 server id 1  end_log_pos 3241 CRC32 0x9e86da30 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497223/*!*/;
BEGIN
/*!*/;
# at 3241
#211214 15:53:43 server id 1  end_log_pos 3310 CRC32 0x7d7a649a 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 3310
#211214 15:53:43 server id 1  end_log_pos 3360 CRC32 0xf87ad1a5 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
B764YRMBAAAARQAAAO4MAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVOaZHp9
B764YR4BAAAAMgAAACANAAAAAPEAAAAAAAEAAgAC/wAKAAAACVJlY2VwY2lvbqXRevg=
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=10
###   @2='Recepcion'
# at 3360
#211214 15:53:43 server id 1  end_log_pos 3391 CRC32 0xe354296f 	Xid = 702881
COMMIT/*!*/;
# at 3391
#211214 15:53:43 server id 1  end_log_pos 3470 CRC32 0xa67e48ee 	Anonymous_GTID	last_committed=10	sequence_number=11	rbr_only=yes	original_committed_timestamp=1639497223738264	immediate_commit_timestamp=1639497223738264	transaction_length=315
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497223738264 (2021-12-14 15:53:43.738264 UTC)
# immediate_commit_timestamp=1639497223738264 (2021-12-14 15:53:43.738264 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497223738264*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 3470
#211214 15:53:43 server id 1  end_log_pos 3554 CRC32 0xa3ebc337 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497223/*!*/;
BEGIN
/*!*/;
# at 3554
#211214 15:53:43 server id 1  end_log_pos 3623 CRC32 0x827311a7 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 3623
#211214 15:53:43 server id 1  end_log_pos 3675 CRC32 0x0816953a 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
B764YRMBAAAARQAAACcOAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVOnEXOC
B764YR4BAAAANAAAAFsOAAAAAPEAAAAAAAEAAgAC/wALAAAAC0xhYm9yYXRvcmlvOpUWCA==
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=11
###   @2='Laboratorio'
# at 3675
#211214 15:53:43 server id 1  end_log_pos 3706 CRC32 0x5debd5ba 	Xid = 702887
COMMIT/*!*/;
# at 3706
#211214 15:53:44 server id 1  end_log_pos 3785 CRC32 0xe02225c2 	Anonymous_GTID	last_committed=11	sequence_number=12	rbr_only=yes	original_committed_timestamp=1639497224185985	immediate_commit_timestamp=1639497224185985	transaction_length=332
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497224185985 (2021-12-14 15:53:44.185985 UTC)
# immediate_commit_timestamp=1639497224185985 (2021-12-14 15:53:44.185985 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497224185985*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 3785
#211214 15:53:44 server id 1  end_log_pos 3869 CRC32 0x70ba54c8 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497224/*!*/;
BEGIN
/*!*/;
# at 3869
#211214 15:53:44 server id 1  end_log_pos 3938 CRC32 0x1c29a49e 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 3938
#211214 15:53:44 server id 1  end_log_pos 4007 CRC32 0x17e1e15e 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
CL64YRMBAAAARQAAAGIPAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVOepCkc
CL64YR4BAAAARQAAAKcPAAAAAPEAAAAAAAEAAgAC/wAMAAAAHEVzdGFjacODwrNuIGRlIHJldmlz
acODwrNuIDFe4eEX
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=12
###   @2='EstaciÃ³n de revisiÃ³n 1'
# at 4007
#211214 15:53:44 server id 1  end_log_pos 4038 CRC32 0x03baba80 	Xid = 702893
COMMIT/*!*/;
# at 4038
#211214 15:53:44 server id 1  end_log_pos 4117 CRC32 0x03d26019 	Anonymous_GTID	last_committed=12	sequence_number=13	rbr_only=yes	original_committed_timestamp=1639497224642759	immediate_commit_timestamp=1639497224642759	transaction_length=332
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497224642759 (2021-12-14 15:53:44.642759 UTC)
# immediate_commit_timestamp=1639497224642759 (2021-12-14 15:53:44.642759 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497224642759*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 4117
#211214 15:53:44 server id 1  end_log_pos 4201 CRC32 0xf0ab3adb 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497224/*!*/;
BEGIN
/*!*/;
# at 4201
#211214 15:53:44 server id 1  end_log_pos 4270 CRC32 0x503f68a0 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 4270
#211214 15:53:44 server id 1  end_log_pos 4339 CRC32 0xae5425bf 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
CL64YRMBAAAARQAAAK4QAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVOgaD9Q
CL64YR4BAAAARQAAAPMQAAAAAPEAAAAAAAEAAgAC/wANAAAAHEVzdGFjacODwrNuIGRlIHJldmlz
acODwrNuIDK/JVSu
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=13
###   @2='EstaciÃ³n de revisiÃ³n 2'
# at 4339
#211214 15:53:44 server id 1  end_log_pos 4370 CRC32 0x99be9897 	Xid = 702899
COMMIT/*!*/;
# at 4370
#211214 15:53:45 server id 1  end_log_pos 4449 CRC32 0xce3f7453 	Anonymous_GTID	last_committed=13	sequence_number=14	rbr_only=yes	original_committed_timestamp=1639497225092017	immediate_commit_timestamp=1639497225092017	transaction_length=332
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497225092017 (2021-12-14 15:53:45.092017 UTC)
# immediate_commit_timestamp=1639497225092017 (2021-12-14 15:53:45.092017 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497225092017*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 4449
#211214 15:53:45 server id 1  end_log_pos 4533 CRC32 0xb5a937a1 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497225/*!*/;
BEGIN
/*!*/;
# at 4533
#211214 15:53:45 server id 1  end_log_pos 4602 CRC32 0xe5389e61 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 4602
#211214 15:53:45 server id 1  end_log_pos 4671 CRC32 0xe829899f 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
Cb64YRMBAAAARQAAAPoRAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVNhnjjl
Cb64YR4BAAAARQAAAD8SAAAAAPEAAAAAAAEAAgAC/wAOAAAAHEVzdGFjacODwrNuIGRlIHJldmlz
acODwrNuIDOfiSno
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=14
###   @2='EstaciÃ³n de revisiÃ³n 3'
# at 4671
#211214 15:53:45 server id 1  end_log_pos 4702 CRC32 0xb074950c 	Xid = 702905
COMMIT/*!*/;
# at 4702
#211214 15:53:45 server id 1  end_log_pos 4781 CRC32 0x028d8861 	Anonymous_GTID	last_committed=14	sequence_number=15	rbr_only=yes	original_committed_timestamp=1639497225544319	immediate_commit_timestamp=1639497225544319	transaction_length=332
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
# original_commit_timestamp=1639497225544319 (2021-12-14 15:53:45.544319 UTC)
# immediate_commit_timestamp=1639497225544319 (2021-12-14 15:53:45.544319 UTC)
/*!80001 SET @@session.original_commit_timestamp=1639497225544319*//*!*/;
/*!80014 SET @@session.original_server_version=80027*//*!*/;
/*!80014 SET @@session.immediate_server_version=80027*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 4781
#211214 15:53:45 server id 1  end_log_pos 4865 CRC32 0xa7b61c85 	Query	thread_id=486	exec_time=0	error_code=0
SET TIMESTAMP=1639497225/*!*/;
BEGIN
/*!*/;
# at 4865
#211214 15:53:45 server id 1  end_log_pos 4934 CRC32 0xd8dbffcf 	Table_map: `PRACTICAUNO`.`HABITACION` mapped to number 241
# at 4934
#211214 15:53:45 server id 1  end_log_pos 5003 CRC32 0x00eb137f 	Write_rows: table id 241 flags: STMT_END_F

BINLOG '
Cb64YRMBAAAARQAAAEYTAAAAAPEAAAAAAAMAC1BSQUNUSUNBVU5PAApIQUJJVEFDSU9OAAIDDwKW
AAIBAQACAVPP/9vY
Cb64YR4BAAAARQAAAIsTAAAAAPEAAAAAAAEAAgAC/wAPAAAAHEVzdGFjacODwrNuIGRlIHJldmlz
acODwrNuIDR/E+sA
'/*!*/;
### INSERT INTO `PRACTICAUNO`.`HABITACION`
### SET
###   @1=15
###   @2='EstaciÃ³n de revisiÃ³n 4'
# at 5003
#211214 15:53:45 server id 1  end_log_pos 5034 CRC32 0xdb3d96b2 	Xid = 702911
COMMIT/*!*/;
# at 5034
#211214 15:54:38 server id 1  end_log_pos 5078 CRC32 0x98327ea6 	Rotate to binlog.000052  pos: 4
SET @@SESSION.GTID_NEXT= 'AUTOMATIC' /* added by mysqlbinlog */ /*!*/;
DELIMITER ;
# End of log file
/*!50003 SET COMPLETION_TYPE=@OLD_COMPLETION_TYPE*/;
/*!50530 SET @@SESSION.PSEUDO_SLAVE_MODE=0*/;

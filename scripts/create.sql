CREATE TABLE PACIENTE(
    idPaciente INT PRIMARY KEY AUTO_INCREMENT,
    edad INT,
    genero VARCHAR(20)
);

CREATE TABLE HABITACION(
    idHabitacion INT PRIMARY KEY AUTO_INCREMENT,
    habitacion VARCHAR(50)
);

CREATE TABLE LOG_HABITACION(
    fk_idHabitacion INT,
    fechatiempo DATETIME,
    estado VARCHAR(45),
    FOREIGN KEY (fk_idHabitacion) REFERENCES HABITACION(idHabitacion)
);


CREATE TABLE LOG_ACTIVIDAD(
    tiempofecha DATETIME,
    actividad VARCHAR(100),
    fk_idHabitacion INT,
    fk_idPaciente INT,
    FOREIGN KEY (fk_idHabitacion) REFERENCES HABITACION(idHabitacion),
    FOREIGN KEY (fk_idPaciente) REFERENCES PACIENTE(idPaciente)
);



